# README #

### What is this repository for? ###

* This repository is a solution for AGL programming challenge

* Programming challenge
* A json web service has been set up at the url: http://agl-developer-test.azurewebsites.net/people.json
* You need to write some code to consume the json and output a list of all the cats in alphabetical order under a heading of the gender of their owner.

* I focused on creating robust and scalable Web API with a very thin Web client

* API uses these technologies
	* Owin
	* Autofac
	* AsyncMediator
	* CQRS
* Web uses these technologies
	* AngularJs
	* Bootstrap

### How do I get set up? ###

* Solution is created using Visual Studio 2017
* Just open it and mark Pet.API and Pet.Web as a start up projects
* Rebuilt and run - all dependencies should be downloaded automatically
* Swagger UI is reachable via http://localhost:4200/swagger