﻿using System;

namespace Pet.Core.Enums
{
    public static class Utilities
    {
        // we can either assume that the enum types coming from json file are static
        // or we can take it as a string and handle it later and try to convert it
        public static T GetEnumFromString<T>(string enumValue)
        {
            return (T)Enum.Parse(typeof(T), enumValue);
        }
    }
}
