﻿namespace Pet.Core.Enums
{
    public enum PetType
    {
        Cat,
        Dog,
        Fish
    }
}
