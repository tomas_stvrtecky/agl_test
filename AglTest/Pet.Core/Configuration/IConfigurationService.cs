﻿namespace Pet.Core.Configuration
{
    public interface IConfigurationService
    {
        string PetUrlEndPoint { get; }
    }
}