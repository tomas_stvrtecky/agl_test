﻿using Autofac;
using Pet.Core.Configuration;
using Pet.Core.Data;
using Module = Autofac.Module;

namespace Pet.Core.Ioc
{
    public class CoreModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {

            builder.RegisterModule<MediatorModule>();

            builder.RegisterType<ConfigurationService>().As<IConfigurationService>().SingleInstance();

            builder.RegisterType<DataRepository>().As<IDataRepository>().SingleInstance();
        }
    }
}
