﻿using Pet.Core.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Pet.Core.Data
{
    public interface IDataRepository
    {
        Task<IList<Owner>> GetOwners();
    }
}
