﻿using System.Collections.Generic;
using Pet.Core.Domain;
using Pet.Core.Configuration;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Pet.Core.Data
{
    public class DataRepository : IDataRepository
    {
        IConfigurationService _configurationService;

        public DataRepository(IConfigurationService configurationService)
        {
            _configurationService = configurationService;
        }

        public async Task<IList<Owner>> GetOwners()
        {
            var client = new HttpClient();
            var response = await client.GetAsync(_configurationService.PetUrlEndPoint);
            response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<Owner[]>(responseBody);
        }
    }
}
