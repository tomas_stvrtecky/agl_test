﻿using Pet.Core.Enums;

namespace Pet.Core.Dtos
{
    public class PetDto
    {
        public string Name { get; set; }
        public PetType Type { get; set; }
        public OwnerDto Owner { get; set; }
    }
}
