﻿using Pet.Core.Enums;

namespace Pet.Core.Dtos
{
    public class OwnerDto
    {
        public string Name { get; set; }
        public Gender Gender { get; set; }
        public int Age { get; set; }
    }
}
