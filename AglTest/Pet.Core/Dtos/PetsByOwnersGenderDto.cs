﻿using System.Collections.Generic;

namespace Pet.Core.Dtos
{
    public class PetsByOwnersGenderDto
    {
        public IList<PetDto> PetsWithMaleOwners { get; set; }
        public IList<PetDto> PetsWithFemaleOwners { get; set; }
    }
}
