﻿using Pet.Core.Enums;
using System.Collections.Generic;

namespace Pet.Core.Domain
{
    public class Owner
    {
        public string Name { get; set; }
        public Gender Gender { get; set; }
        public int Age { get; set; }
        public IList<Pet> Pets { get; set; }
    }
}
