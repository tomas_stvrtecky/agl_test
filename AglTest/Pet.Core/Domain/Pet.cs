﻿using Pet.Core.Enums;

namespace Pet.Core.Domain
{
    public class Pet
    {
        public string Name { get; set; }
        public string Type { get; set; }
    }
}
