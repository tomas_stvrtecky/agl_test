﻿using AsyncMediator;
using Pet.Core.Data;
using Pet.Core.Dtos;
using Pet.Core.Enums;
using System.Linq;
using System.Threading.Tasks;

namespace Pet.Core.Queries.Handlers
{
    public class GetPetsQueryHandler : IQuery<GetPetsQuery, PetsByOwnersGenderDto>
    {
        private readonly IDataRepository _dataRepository;

        public GetPetsQueryHandler(IDataRepository dataRepository)
        {
            _dataRepository = dataRepository;
        }

        public async Task<PetsByOwnersGenderDto> Query(GetPetsQuery criteria)
        {
            
            var allPetsWithOwners = (await _dataRepository.GetOwners())
                .Where(x => x.Pets != null)
                .SelectMany(x => x.Pets
                .Where(y => Utilities.GetEnumFromString<PetType>(y.Type) == criteria.Type)
                .Select(y => new PetDto
                    {
                        Name = y.Name,
                        Type = Utilities.GetEnumFromString<PetType>(y.Type),
                        Owner = new OwnerDto
                        {
                            Name = x.Name,
                            Gender = x.Gender,
                            Age = x.Age
                        }
                    })
                )
                .OrderBy(x => x.Name);

            return new PetsByOwnersGenderDto
            {
                PetsWithMaleOwners = allPetsWithOwners.Where(x => x.Owner.Gender == Gender.Male).ToList(),
                PetsWithFemaleOwners = allPetsWithOwners.Where(x => x.Owner.Gender == Gender.Female).ToList()
            };
        }
    }
}
