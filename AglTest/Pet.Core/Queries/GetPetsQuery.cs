﻿using Pet.Core.Enums;

namespace Pet.Core.Queries
{
    public class GetPetsQuery
    {
        public PetType Type { get; set; }

        public GetPetsQuery(PetType type)
        {
            Type = type;
        }
    }
}
