﻿using AsyncMediator;
using Pet.Core.Dtos;
using Pet.Core.Enums;
using Pet.Core.Queries;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace Pet.API.Controllers
{
    public class PetController : ApiController
    {
        IMediator _mediator;
        public PetController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [ResponseType(typeof(PetsByOwnersGenderDto))]
        [Route("pet")]
        [HttpGet]
        public async Task<IHttpActionResult> Get(PetType type)
        {
            var result = await _mediator.Query<GetPetsQuery, PetsByOwnersGenderDto>(new GetPetsQuery(type));
            return result == null ? (IHttpActionResult)NotFound() : Ok(result);
        }
    }
}
