﻿using Microsoft.Owin;
using Owin;
using Pet.API;
using System.Web.Http;

[assembly: OwinStartup(typeof(Startup))]
namespace Pet.API
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();
            WebApiConfig.Register(config);

            app.UseWebApi(config);
        }
    }
}