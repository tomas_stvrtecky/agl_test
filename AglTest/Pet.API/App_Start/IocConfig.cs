﻿using Autofac;
using Autofac.Integration.WebApi;
using Pet.Core.Ioc;
using System.Reflection;
using System.Web.Http;

namespace Pet.API
{
    public class IocConfig
    {
        public static void Configure(HttpConfiguration config)
        {
            var builder = new ContainerBuilder();

            // Register your Web API controllers.
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterModule<CoreModule>();

            var container = builder.Build();
            //Register as dependency resolver for WebAPI
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}