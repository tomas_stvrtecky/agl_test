﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System.Web.Http;

namespace Pet.API
{
    public class JsonConfig
    {
        public static void Configure(HttpConfiguration config)
        {

            var formatters = config.Formatters;
            var jsonFormatter = formatters.JsonFormatter;
            var settings = jsonFormatter.SerializerSettings;
            //api should only serve json
            formatters.Remove(config.Formatters.XmlFormatter);
            //make json text indented for readability
            settings.Formatting = Formatting.Indented;
            //property names will be camel case as javascript standards
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            //json will render enums as strings for readability and easy discovery
            settings.Converters.Add(new StringEnumConverter());
            //share same settings between web api and manually calling JsonConvert
            JsonConvert.DefaultSettings = () => settings;
        }
    }
}