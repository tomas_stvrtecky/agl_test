﻿using System.Web.Http;

namespace Pet.API
{
    public class RoutingConfig
    {
        public static void Configure(HttpConfiguration config)
        {
            //use attribute routing
            config.MapHttpAttributeRoutes();
        }
    }
}