﻿using System.Web.Http;
using System.Web.Http.Cors;

namespace Pet.API
{
    public class CorsConfig
    {
        public static void Configure(HttpConfiguration config)
        {
            //allow  Cross Origin Resource Sharing (CORS) from any domain
            var cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);
        }
    }
}