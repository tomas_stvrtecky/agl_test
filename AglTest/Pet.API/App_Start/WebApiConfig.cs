﻿using System.Web.Http;

namespace Pet.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            RoutingConfig.Configure(config);
            SwaggerConfig.Register(config);
            IocConfig.Configure(config);
            JsonConfig.Configure(config);
            CorsConfig.Configure(config);
        }
    }
}
