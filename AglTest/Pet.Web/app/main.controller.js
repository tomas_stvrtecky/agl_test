﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('main.controller', [
            "$http", "$log", "data.service", function ($http, $log, dataService) {

                var vm = this;
                vm.petType = "cat";

                vm.load = function (petType) {
                    vm.petType = petType;

                    dataService.getPets(vm.petType).then(function (result) {
                        vm.pets = result;
                    }, function () {
                        alert("Unexpected error occured.");
                    });
                }

                vm.load(vm.petType);
            }
        ]);
})();