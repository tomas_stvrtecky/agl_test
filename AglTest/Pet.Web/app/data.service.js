﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('data.service', [
            "$http", "$log", function ($http, $log) {

                function getPets(petType) {
                    return $http.get("http://localhost:4200/pet?type=" + petType)
                        .then(function (response) {
                            return response.data;
                        }, function (response) {
                            $log.debug(response);
                        });
                }

                return {
                    getPets: getPets
                };
            }
        ]);
})();