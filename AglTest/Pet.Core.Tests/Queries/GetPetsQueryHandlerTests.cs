﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pet.Core.Queries.Handlers;
using NSubstitute;
using Pet.Core.Data;
using Pet.Core.Domain;
using System.Collections.Generic;
using Newtonsoft.Json;
using Pet.Core.Queries;
using System.Threading.Tasks;
using Pet.Core.Enums;

namespace Pet.Core.Tests.Queries
{
    [TestClass]
    public class GetPetsQueryHandlerTests
    {
        private readonly GetPetsQueryHandler _handler;
        readonly IList<Owner> _owners;

        public GetPetsQueryHandlerTests()
        {
            var repository = Substitute.For<IDataRepository>();

            _owners = SetUpMockData();
            repository.GetOwners().Returns(_owners);

            _handler = new GetPetsQueryHandler(repository);
        }

        private IList<Owner> SetUpMockData()
        {
            return JsonConvert.DeserializeObject<Owner[]>("[{'name':'Bob','gender':'Male','age':23,'pets':[{'name':'Garfield','type':'Cat'},{'name':'Fido','type':'Dog'}]},{'name':'Jennifer','gender':'Female','age':18,'pets':[{'name':'Garfield','type':'Cat'}]},{'name':'Steve','gender':'Male','age':45,'pets':null},{'name':'Fred','gender':'Male','age':40,'pets':[{'name':'Tom','type':'Cat'},{'name':'Max','type':'Cat'},{'name':'Sam','type':'Dog'},{'name':'Jim','type':'Cat'}]},{'name':'Samantha','gender':'Female','age':40,'pets':[{'name':'Tabby','type':'Cat'}]},{'name':'Alice','gender':'Female','age':64,'pets':[{'name':'Simba','type':'Cat'},{'name':'Nemo','type':'Fish'}]}]");
        }

        [TestMethod]
        public async Task GetPetsQueryHandler_ForPetTypeCat_ReturnsDataInCorrectOrder()
        {
            //Arrange
            var query = new GetPetsQuery(PetType.Cat);

            //Act
            var result = await _handler.Query(query);

            //Assert
            Assert.AreEqual(4, result.PetsWithMaleOwners.Count);
            Assert.AreEqual(3, result.PetsWithFemaleOwners.Count);

            Assert.AreEqual("Garfield", result.PetsWithMaleOwners[0].Name);
            Assert.AreEqual(PetType.Cat, result.PetsWithMaleOwners[0].Type);
            Assert.AreEqual("Bob", result.PetsWithMaleOwners[0].Owner.Name);
            Assert.AreEqual(Gender.Male, result.PetsWithMaleOwners[0].Owner.Gender);
            Assert.AreEqual(23, result.PetsWithMaleOwners[0].Owner.Age);

            Assert.AreEqual("Jim", result.PetsWithMaleOwners[1].Name);
            Assert.AreEqual(PetType.Cat, result.PetsWithMaleOwners[1].Type);
            Assert.AreEqual("Fred", result.PetsWithMaleOwners[1].Owner.Name);
            Assert.AreEqual(Gender.Male, result.PetsWithMaleOwners[1].Owner.Gender);
            Assert.AreEqual(40, result.PetsWithMaleOwners[1].Owner.Age);

            Assert.AreEqual("Max", result.PetsWithMaleOwners[2].Name);
            Assert.AreEqual(PetType.Cat, result.PetsWithMaleOwners[2].Type);
            Assert.AreEqual("Fred", result.PetsWithMaleOwners[2].Owner.Name);
            Assert.AreEqual(Gender.Male, result.PetsWithMaleOwners[2].Owner.Gender);
            Assert.AreEqual(40, result.PetsWithMaleOwners[2].Owner.Age);

            Assert.AreEqual("Tom", result.PetsWithMaleOwners[3].Name);
            Assert.AreEqual(PetType.Cat, result.PetsWithMaleOwners[3].Type);
            Assert.AreEqual("Fred", result.PetsWithMaleOwners[3].Owner.Name);
            Assert.AreEqual(Gender.Male, result.PetsWithMaleOwners[3].Owner.Gender);
            Assert.AreEqual(40, result.PetsWithMaleOwners[3].Owner.Age);

            Assert.AreEqual("Garfield", result.PetsWithFemaleOwners[0].Name);
            Assert.AreEqual(PetType.Cat, result.PetsWithFemaleOwners[0].Type);
            Assert.AreEqual("Jennifer", result.PetsWithFemaleOwners[0].Owner.Name);
            Assert.AreEqual(Gender.Female, result.PetsWithFemaleOwners[0].Owner.Gender);
            Assert.AreEqual(18, result.PetsWithFemaleOwners[0].Owner.Age);

            Assert.AreEqual("Simba", result.PetsWithFemaleOwners[1].Name);
            Assert.AreEqual(PetType.Cat, result.PetsWithFemaleOwners[1].Type);
            Assert.AreEqual("Alice", result.PetsWithFemaleOwners[1].Owner.Name);
            Assert.AreEqual(Gender.Female, result.PetsWithFemaleOwners[1].Owner.Gender);
            Assert.AreEqual(64, result.PetsWithFemaleOwners[1].Owner.Age);

            Assert.AreEqual("Tabby", result.PetsWithFemaleOwners[2].Name);
            Assert.AreEqual(PetType.Cat, result.PetsWithFemaleOwners[2].Type);
            Assert.AreEqual("Samantha", result.PetsWithFemaleOwners[2].Owner.Name);
            Assert.AreEqual(Gender.Female, result.PetsWithFemaleOwners[2].Owner.Gender);
            Assert.AreEqual(40, result.PetsWithFemaleOwners[2].Owner.Age);
        }

        [TestMethod]
        public async Task GetPetsQueryHandler_ForPetTypeDog_ReturnsDataInCorrectOrder()
        {
            //Arrange
            var query = new GetPetsQuery(PetType.Dog);

            //Act
            var result = await _handler.Query(query);

            //Assert
            Assert.AreEqual(2, result.PetsWithMaleOwners.Count);
            Assert.AreEqual(0, result.PetsWithFemaleOwners.Count);

            Assert.AreEqual("Fido", result.PetsWithMaleOwners[0].Name);
            Assert.AreEqual(PetType.Dog, result.PetsWithMaleOwners[0].Type);
            Assert.AreEqual("Bob", result.PetsWithMaleOwners[0].Owner.Name);
            Assert.AreEqual(Gender.Male, result.PetsWithMaleOwners[0].Owner.Gender);
            Assert.AreEqual(23, result.PetsWithMaleOwners[0].Owner.Age);

            Assert.AreEqual("Sam", result.PetsWithMaleOwners[1].Name);
            Assert.AreEqual(PetType.Dog, result.PetsWithMaleOwners[1].Type);
            Assert.AreEqual("Fred", result.PetsWithMaleOwners[1].Owner.Name);
            Assert.AreEqual(Gender.Male, result.PetsWithMaleOwners[1].Owner.Gender);
            Assert.AreEqual(40, result.PetsWithMaleOwners[1].Owner.Age);
        }

        [TestMethod]
        public async Task GetPetsQueryHandler_ForPetTypeFish_ReturnsDataInCorrectOrder()
        {
            //Arrange
            var query = new GetPetsQuery(PetType.Fish);

            //Act
            var result = await _handler.Query(query);

            //Assert
            Assert.AreEqual(0, result.PetsWithMaleOwners.Count);
            Assert.AreEqual(1, result.PetsWithFemaleOwners.Count);

            Assert.AreEqual("Nemo", result.PetsWithFemaleOwners[0].Name);
            Assert.AreEqual(PetType.Fish, result.PetsWithFemaleOwners[0].Type);
            Assert.AreEqual("Alice", result.PetsWithFemaleOwners[0].Owner.Name);
            Assert.AreEqual(Gender.Female, result.PetsWithFemaleOwners[0].Owner.Gender);
            Assert.AreEqual(64, result.PetsWithFemaleOwners[0].Owner.Age);
        }
    }
}
